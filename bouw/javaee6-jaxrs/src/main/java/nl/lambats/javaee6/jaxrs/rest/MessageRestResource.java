/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.lambats.javaee6.jaxrs.rest;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author harald
 */
//@Path("/message")
@Path("/message/{messageid}")
public class MessageRestResource {
 
	@GET
        @Produces(MediaType.TEXT_PLAIN)
	public String getMessage(@PathParam("messageid") String messageId) {
 
		return "Restful example : " + messageId;
 
	}
 
}
