/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.belastingandmore.business.security.boundary;

import java.security.Principal;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;

/**
 *
 * @author adam-bien.com
 */
public class PrincipalProvider {

    @Inject
    Principal p;

    @Produces
    public BelastingPrincipal expose() {

        return new BelastingPrincipal(p.getName());
    }
}
