/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.belastingandmore.business.configuration.boundary;

import javax.enterprise.inject.Produces;
import javax.enterprise.inject.spi.InjectionPoint;

/**
 *
 * @author adam-bien.com
 */
public class ConfigurationProvider {

    @Produces
    public String configure(InjectionPoint ip) {
        Class<?> declaringClass = ip.getMember().getDeclaringClass();
        String name = ip.getMember().getName();
        return declaringClass.getName() + "->" + name + " here custom message from DB";
    }

}
