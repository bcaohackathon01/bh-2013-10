package nl.belastingandmore.business;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 *
 * @author adam-bien.com
 */
@ApplicationPath("v1")
public class RESTConfiguration extends Application {

}
