package nl.belastingandmore.business.tax.control;

import java.util.Date;
import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.ejb.ConcurrencyManagement;
import javax.ejb.ConcurrencyManagementType;
import javax.ejb.ScheduleExpression;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.ejb.Timeout;
import javax.ejb.Timer;
import javax.ejb.TimerService;
import javax.inject.Inject;

/**
 *
 * @author adam-bien.com
 */
@Startup
@Singleton
@ConcurrencyManagement(ConcurrencyManagementType.BEAN)
public class TaxAbo {

    @Resource
    TimerService ts;
    private Timer timer;

    @Inject
    private String message;

    @PostConstruct
    public void initialize() {
        ScheduleExpression se = new ScheduleExpression();
        se.hour("*").minute("*").second("*/5");
        this.timer = ts.createCalendarTimer(se);
    }

    @Timeout
    public void chargeMe() {
        System.out.println(this.message + new Date());
    }
}
