/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.belastingandmore.business.tax.boundary;

import javax.json.Json;
import javax.json.JsonObject;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;

/**
 *
 * @author adam-bien.com
 */
@Path("customers")
public class CustomersResource {

    @GET
    public JsonObject get() {
        return Json.createObjectBuilder().add("name", "duke").build();
    }

    @POST
    public void save(JsonObject object) {
        System.out.println("got " + object);
    }

}
