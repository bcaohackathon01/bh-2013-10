package nl.belastingandmore.presentation;

import javax.interceptor.AroundInvoke;
import javax.interceptor.InvocationContext;

/**
 *
 * @author adam-bien.com
 */
public class AuditTraffic {

    @AroundInvoke
    public Object audit(InvocationContext ic) throws Exception {
        System.out.println("Before method: " + ic.getMethod());
        return ic.proceed();
    }
}
