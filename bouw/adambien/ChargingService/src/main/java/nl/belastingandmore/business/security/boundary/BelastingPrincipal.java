/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.belastingandmore.business.security.boundary;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author adam-bien.com
 */
public class BelastingPrincipal {

    private String name;

    private List<String> entitlements;

    public BelastingPrincipal(String name) {
        this.entitlements = new ArrayList<>();
        this.name = name;
    }

    public void add(String action) {
        this.entitlements.add(action);
    }

}
