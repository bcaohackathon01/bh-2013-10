package nl.belastingandmore.business.tax.boundary;

import java.util.concurrent.Future;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.AsyncResult;
import javax.ejb.Asynchronous;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import nl.belastingandmore.business.tax.control.Validation;
import nl.belastingandmore.business.tax.entity.Belasting;
import nl.belastingandmore.presentation.AuditTraffic;

/**
 *
 * @author adam-bien.com
 */
@Interceptors({AuditTraffic.class})
@Stateless
public class BelastingClerk {

    @Inject
    Validation validation;

    @PersistenceContext
    EntityManager em;

    @PostConstruct
    public void init() {
        System.out.println("Creating an EJB");
    }

    public String heyJoe() {
        return "not joke";
    }

    @Asynchronous
    public Future<String> justDoIt(int amount) {
        em.merge(new Belasting(amount));
        try {
            Thread.sleep(2000);
        } catch (InterruptedException ex) {
            Logger.getLogger(BelastingClerk.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println("The amount is: " + amount + validation.isOk(amount));
        return new AsyncResult<>("42");
    }
}
