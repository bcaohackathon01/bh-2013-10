package nl.belastingandmore.business.tax.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author adam-bien.com
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement
@Entity
public class Belasting {

    @Id
    @GeneratedValue
    private long id;
    private int amount;

    public Belasting() {
    }

    public Belasting(int amount) {
        this.amount = amount;
    }

    @Override
    public String toString() {
        return "Belasting{" + "id=" + id + ", amount=" + amount + '}';
    }

}
