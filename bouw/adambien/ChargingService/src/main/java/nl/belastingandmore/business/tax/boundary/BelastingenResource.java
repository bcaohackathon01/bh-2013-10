package nl.belastingandmore.business.tax.boundary;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import nl.belastingandmore.business.security.boundary.BelastingPrincipal;
import nl.belastingandmore.business.tax.entity.Belasting;

/**
 *
 * @author adam-bien.com
 */
@Stateless
@Path("belastingen")
public class BelastingenResource {

    @Inject
    BelastingPrincipal principal;

    @GET
    public List<Belasting> all() {
        List<Belasting> belastingen = new ArrayList<>();
        belastingen.add(new Belasting(21));
        belastingen.add(new Belasting(42));
        return belastingen;
    }

    @GET
    @Path("{id}")
    public Belasting particular(@PathParam("id") int id) {
        System.out.println("-- " + principal);
        return new Belasting(id);
    }

    @POST
    public Response save(Belasting belasting, @Context UriInfo info, @Context HttpHeaders headers) {
        System.out.println("I'm glad: " + belasting);
        URI uri = URI.create("/" + System.currentTimeMillis());
        return Response.created(uri).build();
    }
}
