package nl.belastingandmore.presentation;

import java.io.IOException;
import javax.inject.Inject;
import javax.servlet.AsyncContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import nl.belastingandmore.business.tax.boundary.BelastingClerk;

/**
 *
 * @author adam-bien.com
 */
@WebServlet(name = "BelastingServlet", urlPatterns = {"/BelastingServlet"}, asyncSupported = true)
public class BelastingServlet extends HttpServlet {

    @Inject
    private BelastingClerk bc;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        bc.justDoIt(42);
        AsyncContext asyncContext = request.startAsync();
        asyncContext.getResponse().getWriter().print("You are charged asynchronously!");
        asyncContext.complete();

    }

}
