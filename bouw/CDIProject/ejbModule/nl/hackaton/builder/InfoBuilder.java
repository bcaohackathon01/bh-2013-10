package nl.hackaton.builder;

import java.util.Map;

import javax.annotation.PostConstruct;

/**
 * POYO builder object. Wordt geinjecteerd in InfoService.
 * 
 * @author fokke
 */
public class InfoBuilder {

	private String state = "-none-";
	private Map<String, String> commands;
	
	@PostConstruct
	public void buildBuilder() {
		this.state = "An arbitrary state";
	}

	public void setCommands(Map<String, String> commands) {
		this.commands = commands;
	}

	public String getInfo() {
		StringBuilder builder = new StringBuilder();

		builder.append("[InfoBuilderInfo]<br>");
		builder.append("State:" + state + "<br>");
		if (commands != null) {
			for (String key : commands.keySet()) {
				builder.append(String.format("Key %s and value %s.<br>", key,
						commands.get(key)));
			}
		}

		return builder.toString();

	}

}
