package nl.hackaton.parser;

import java.util.HashMap;
import java.util.Map;

/**
 * POYO Object wordt geinjecteerd in InfoService.
 * 
 * @author fokke
 * 
 */
public class InfoParser {

	/**
	 * Doet wat arbitraire "parsing". Zoekt naar key-value paar gescheiden door
	 * whitespace.
	 * 
	 * @param infoString
	 *            een arbitraire inputstring
	 * @return Map met key-value paren.
	 */
	public Map<String, String> parse(String infoString) {
		Map<String, String> commands = new HashMap<>();

		String[] tuples = infoString.split("\\s");
		for (String tuple : tuples) {
			String[] keyValue = tuple.split("=");
			if (keyValue.length == 2) {
				commands.put(keyValue[0], keyValue[1]);
			}
		}

		return commands;
	}

}
