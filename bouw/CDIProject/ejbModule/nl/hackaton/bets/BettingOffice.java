package nl.hackaton.bets;

import javax.faces.bean.ApplicationScoped;
import javax.inject.Inject;

/**
 * POYO class tbv het bijhouden van een gegenereerd random getal. Let op de
 * 
 * "@SessionScoped". Dit betekend dat gedurende de loop van de applicatie deze
 * bean blijft bestaan.
 * 
 * @author fokke
 */
@ApplicationScoped
public class BettingOffice {

	private int currentValue;

	@Inject
	private Generator generator;
	
	public boolean makeGuess(int value) {
		if (value == currentValue) {
			// Select new number when correct number has been guessed.
			currentValue = generator.next();

			return true;
		} else {
			return false;
		}
	}

	public int getCurrentValue() {
		return currentValue;
	}

}
