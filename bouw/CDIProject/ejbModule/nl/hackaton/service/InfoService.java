package nl.hackaton.service;

import java.util.Map;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.jws.WebService;

import nl.hackaton.bets.BettingOffice;
import nl.hackaton.builder.InfoBuilder;
import nl.hackaton.parser.InfoParser;

/**
 * Infoservice is een WebService waar door middel van CDI (@Inject) gewone
 * POYO's in worden gehangen. <br>
 * <br>
 * Wanneer je gebruik maakt van GlassFish kun je, via de adminconsole, gebruik
 * maken van een testclient.
 * 
 * @author fokke
 */
@WebService
@Stateless
public class InfoService {

	@Inject
	private InfoParser infoParser;
	@Inject
	private InfoBuilder infoBuilder;
	@Inject
	private BettingOffice bettingOffice;

	public String echo(String message) {
		return String.format("Echo: %s", message);
	}

	public String getInfo(String input) {
		Map<String, String> commands = infoParser.parse(input);
		infoBuilder.setCommands(commands);
		return infoBuilder.getInfo();
	}

	public String placeBet(int valueBetween0and10) {
		if (bettingOffice.makeGuess(valueBetween0and10)) {
			return "Well done, the nuber you chose was correct! New rounds, new chances.. Pick a number ranging from 0 to 9!";
		} else {
			return "I'm sorry, you guessed wrong. Want to give it another try? Pick a number, any number, ranging from 0 to 9!" + " (try " + bettingOffice.getCurrentValue() + ")";
		}
	}

}
