/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package nl.lambats.javaee6.hackathon.fun.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author harald
 */
@Entity
@Table(name = "vogelspot")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Vogelspot.findAll", query = "SELECT v FROM Vogelspot v"),
    @NamedQuery(name = "Vogelspot.findById", query = "SELECT v FROM Vogelspot v WHERE v.id = :id"),
    @NamedQuery(name = "Vogelspot.findByVogelId", query = "SELECT v FROM Vogelspot v WHERE v.vogelId = :vogelId"),
    @NamedQuery(name = "Vogelspot.findByLocatie", query = "SELECT v FROM Vogelspot v WHERE v.locatie = :locatie"),
    @NamedQuery(name = "Vogelspot.findByTijdwaarneming", query = "SELECT v FROM Vogelspot v WHERE v.tijdwaarneming = :tijdwaarneming"),
    @NamedQuery(name = "Vogelspot.findByAantal", query = "SELECT v FROM Vogelspot v WHERE v.aantal = :aantal"),
    @NamedQuery(name = "Vogelspot.findByVogelfotoId", query = "SELECT v FROM Vogelspot v WHERE v.vogelfotoId = :vogelfotoId"),
    @NamedQuery(name = "Vogelspot.findByVogelaarId", query = "SELECT v FROM Vogelspot v WHERE v.vogelaarId = :vogelaarId")})
public class Vogelspot implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "vogel_id")
    private int vogelId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "locatie")
    private String locatie;
    @Basic(optional = false)
    @NotNull
    @Column(name = "tijdwaarneming")
    @Temporal(TemporalType.DATE)
    private Date tijdwaarneming;
    @Column(name = "aantal")
    private Integer aantal;
    @Column(name = "vogelfoto_id")
    private Integer vogelfotoId;
    @Column(name = "vogelaar_id")
    private Integer vogelaarId;

    public Vogelspot() {
    }

    public Vogelspot(Integer id) {
        this.id = id;
    }

    public Vogelspot(Integer id, int vogelId, String locatie, Date tijdwaarneming) {
        this.id = id;
        this.vogelId = vogelId;
        this.locatie = locatie;
        this.tijdwaarneming = tijdwaarneming;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public int getVogelId() {
        return vogelId;
    }

    public void setVogelId(int vogelId) {
        this.vogelId = vogelId;
    }

    public String getLocatie() {
        return locatie;
    }

    public void setLocatie(String locatie) {
        this.locatie = locatie;
    }

    public Date getTijdwaarneming() {
        return tijdwaarneming;
    }

    public void setTijdwaarneming(Date tijdwaarneming) {
        this.tijdwaarneming = tijdwaarneming;
    }

    public Integer getAantal() {
        return aantal;
    }

    public void setAantal(Integer aantal) {
        this.aantal = aantal;
    }

    public Integer getVogelfotoId() {
        return vogelfotoId;
    }

    public void setVogelfotoId(Integer vogelfotoId) {
        this.vogelfotoId = vogelfotoId;
    }

    public Integer getVogelaarId() {
        return vogelaarId;
    }

    public void setVogelaarId(Integer vogelaarId) {
        this.vogelaarId = vogelaarId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Vogelspot)) {
            return false;
        }
        Vogelspot other = (Vogelspot) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "nl.lambats.javaee6.hackathon.fun.entity.Vogelspot[ id=" + id + " ]";
    }
    
}
