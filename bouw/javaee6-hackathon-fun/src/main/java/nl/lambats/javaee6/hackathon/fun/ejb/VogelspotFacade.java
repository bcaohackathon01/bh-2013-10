/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package nl.lambats.javaee6.hackathon.fun.ejb;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import nl.lambats.javaee6.hackathon.fun.entity.Vogelspot;

/**
 *
 * @author harald
 */
@Stateless
public class VogelspotFacade extends AbstractFacade<Vogelspot> {
    @PersistenceContext(unitName = "nl.lambats.javaee6.hackathon.fun_javaee6-hackathon-fun_war_0.0.0-SNAPSHOTPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public VogelspotFacade() {
        super(Vogelspot.class);
    }
    
}
