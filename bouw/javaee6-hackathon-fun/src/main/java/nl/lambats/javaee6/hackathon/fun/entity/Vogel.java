/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package nl.lambats.javaee6.hackathon.fun.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author harald
 */
@Entity
@Table(name = "vogel")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Vogel.findAll", query = "SELECT v FROM Vogel v"),
    @NamedQuery(name = "Vogel.findById", query = "SELECT v FROM Vogel v WHERE v.id = :id"),
    @NamedQuery(name = "Vogel.findByNaam", query = "SELECT v FROM Vogel v WHERE v.naam = :naam"),
    @NamedQuery(name = "Vogel.findBySoort", query = "SELECT v FROM Vogel v WHERE v.soort = :soort"),
    @NamedQuery(name = "Vogel.findByLeefgebied", query = "SELECT v FROM Vogel v WHERE v.leefgebied = :leefgebied"),
    @NamedQuery(name = "Vogel.findByMigrate", query = "SELECT v FROM Vogel v WHERE v.migrate = :migrate"),
    @NamedQuery(name = "Vogel.findByInfo", query = "SELECT v FROM Vogel v WHERE v.info = :info"),
    @NamedQuery(name = "Vogel.findByVogelfotoIdMan", query = "SELECT v FROM Vogel v WHERE v.vogelfotoIdMan = :vogelfotoIdMan"),
    @NamedQuery(name = "Vogel.findByVogelfotoIdVrouw", query = "SELECT v FROM Vogel v WHERE v.vogelfotoIdVrouw = :vogelfotoIdVrouw"),
    @NamedQuery(name = "Vogel.findByVogelfotoIdJong", query = "SELECT v FROM Vogel v WHERE v.vogelfotoIdJong = :vogelfotoIdJong")})
public class Vogel implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 25)
    @Column(name = "naam")
    private String naam;
    @Size(max = 25)
    @Column(name = "soort")
    private String soort;
    @Size(max = 100)
    @Column(name = "leefgebied")
    private String leefgebied;
    @Column(name = "migrate")
    private Boolean migrate;
    @Size(max = 250)
    @Column(name = "info")
    private String info;
    @Column(name = "vogelfoto_id_man")
    private Integer vogelfotoIdMan;
    @Column(name = "vogelfoto_id_vrouw")
    private Integer vogelfotoIdVrouw;
    @Column(name = "vogelfoto_id_jong")
    private Integer vogelfotoIdJong;

    public Vogel() {
    }

    public Vogel(Integer id) {
        this.id = id;
    }

    public Vogel(Integer id, String naam) {
        this.id = id;
        this.naam = naam;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNaam() {
        return naam;
    }

    public void setNaam(String naam) {
        this.naam = naam;
    }

    public String getSoort() {
        return soort;
    }

    public void setSoort(String soort) {
        this.soort = soort;
    }

    public String getLeefgebied() {
        return leefgebied;
    }

    public void setLeefgebied(String leefgebied) {
        this.leefgebied = leefgebied;
    }

    public Boolean getMigrate() {
        return migrate;
    }

    public void setMigrate(Boolean migrate) {
        this.migrate = migrate;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public Integer getVogelfotoIdMan() {
        return vogelfotoIdMan;
    }

    public void setVogelfotoIdMan(Integer vogelfotoIdMan) {
        this.vogelfotoIdMan = vogelfotoIdMan;
    }

    public Integer getVogelfotoIdVrouw() {
        return vogelfotoIdVrouw;
    }

    public void setVogelfotoIdVrouw(Integer vogelfotoIdVrouw) {
        this.vogelfotoIdVrouw = vogelfotoIdVrouw;
    }

    public Integer getVogelfotoIdJong() {
        return vogelfotoIdJong;
    }

    public void setVogelfotoIdJong(Integer vogelfotoIdJong) {
        this.vogelfotoIdJong = vogelfotoIdJong;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Vogel)) {
            return false;
        }
        Vogel other = (Vogel) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "nl.lambats.javaee6.hackathon.fun.entity.Vogel[ id=" + id + " ]";
    }
    
}
