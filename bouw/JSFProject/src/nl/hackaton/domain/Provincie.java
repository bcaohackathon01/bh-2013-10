package nl.hackaton.domain;

/**
 * Een enum om te gebruiken in de "events.xhtml" en de "OtherManagedBean". <br>
 * PS. Ja, Friesland staat terecht bovenaan Patrick!
 * 
 * @author fokke
 */
public enum Provincie {
	FRIESLAND("Friesland"), GRONINGEN("Groningen"), DRENTHE("Drenthe"), NOORTHOLLAND(
			"Noordholland"), ZUIDHOLLAND("Zuidholland"), OVERIJSSEL("Overijssel"), FLEVOLAND(
			"Flevoland"), ZEELAND("Zeeland"), BRABANT("Brabant"), LIMBURG(
			"Limburg"), GELDERLAND("Gelderland"), UTRECHT("Utrecht"), UNKNOWN("-onbekend-");

	private String name;

	Provincie(String name) {
		this.name = name;
	}

	public String toString() {
		return name;
	}
}
