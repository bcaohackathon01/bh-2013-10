package nl.hackaton.beans.jsf;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.event.AbortProcessingException;
import javax.faces.event.ValueChangeEvent;

import nl.hackaton.domain.Provincie;

/**
 * Handelt events af die door events.xhtml worden gegenereerd (en houdt deze
 * bij).
 * 
 * @author fokke
 */
@ManagedBean
@ApplicationScoped
public class OtherManagedBean {

	private Provincie provincie = Provincie.UNKNOWN;

	public Provincie getProvincie() {
		return provincie;
	}

	public void setProvincie(Provincie provincie) {
		this.provincie = provincie;
	}

	/**
	 * Alternatief voor deze "changed" methode is het schrijven van een
	 * full-fledged ValueChangeListener (
	 * http://www.tutorialspoint.com/jsf/jsf_valuechangelistener_tag.htm )
	 */
	public void provincieChanged(ValueChangeEvent e)
			throws AbortProcessingException {
		Logger.getLogger("anyLogger").log(
				Level.INFO,
				String.format("Value changed from %s to %s.", e.getOldValue(),
						e.getNewValue()));
	}

	public List<Provincie> getProvincien() {
		ArrayList<Provincie> provincien = new ArrayList<Provincie>(
				Arrays.asList(Provincie.values()));
		// Wie woont er nu in Limburg?
		provincien.remove(Provincie.LIMBURG);

		return provincien;
	}

}
