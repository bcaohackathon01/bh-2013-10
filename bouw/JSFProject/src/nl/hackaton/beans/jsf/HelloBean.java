package nl.hackaton.beans.jsf;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;

import nl.hackaton.beans.ejb.ServiceBeanRemote;

/**
 * Session bean (JSF) \@ManagedBeanmanaged. Wordt automagisch aangemaakt en
 * blijft gedurende de \@SessionScoped actief.<br>
 * Maakt tevens gebruik van zowel een EJB als een andere ManagedProperty
 * 
 * @author fokke
 */
@ManagedBean
@SessionScoped
public class HelloBean {
	
	/* Managed beans kunnen van EJBs gebruik maken */
	@EJB
	private ServiceBeanRemote serviceBean;
	
	/* Managed beans kunnen onderling gebruik van elkaar maken */
	@ManagedProperty(value = "#{otherManagedBean}")
	private OtherManagedBean otherManagedBean;

	/* name en password worden gebruikt door login.xhtml */
	private String name;
	private String password;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void setOtherManagedBean(OtherManagedBean otherManagedBean) {
		this.otherManagedBean = otherManagedBean;
	}

	public String getLastname() {
		return serviceBean.getLastname();
	}

	public String getProvincie() {
		return otherManagedBean.getProvincie().toString();
	}
}
