package nl.hackaton.beans.ejb;

import javax.ejb.Local;

@Local
public interface ServiceBeanLocal {

	String getLastname();
}
