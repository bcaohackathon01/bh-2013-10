package nl.hackaton.beans.ejb;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;

/**
 * Session Bean implementation class ServiceBean
 */
@Stateless
@LocalBean
public class ServiceBean implements ServiceBeanRemote, ServiceBeanLocal {

    /**
     * Default constructor. 
     */
    public ServiceBean() {
        // TODO Auto-generated constructor stub
    }

	@Override
	public String getLastname() {
		return "<achternaam uit ServiceBean (EJB)>";
	}

}
