package nl.hackaton.validator;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

import org.apache.commons.lang3.StringUtils;

/**
 * Dit is een voorbeeld van een custom-validator. Wordt gebruikt in login.xhtml.
 * <br>
 * Niet vergeten: input-validatie moet OOK altijd server-side!
 * 
 * @author fokke
 */
@FacesValidator("nl.hackaton.validator.UsernameValidator")
public class UsernameValidator implements Validator {

	@Override
	public void validate(FacesContext context, UIComponent component,
			Object value) throws ValidatorException {
		String username = (String) value;

		String msg = null;
		if (StringUtils.isBlank(username)) {
			msg = "Username moet gevuld zijn (UsernameValidator.java).";
		} else if (username.length() > 12) {
			msg = "Username moet korter zijn dan 12 karakters (UsernameValidator.java).";
		}

		if (msg != null) {
			FacesMessage message = getMessage(msg);
			context.addMessage("userForm:Email", message);
			throw new ValidatorException(message);
		}
	}

	private FacesMessage getMessage(String msg) {
		FacesMessage message = new FacesMessage();
		message.setSeverity(FacesMessage.SEVERITY_ERROR);
		message.setSummary(msg);
		message.setDetail(msg);
		
		return message;
	}

}
