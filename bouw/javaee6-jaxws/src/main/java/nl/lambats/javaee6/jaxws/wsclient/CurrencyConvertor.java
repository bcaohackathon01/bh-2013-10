/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package nl.lambats.javaee6.jaxws.wsclient;

import javax.ejb.Stateless;
import javax.xml.ws.ServiceMode;
import javax.xml.ws.WebServiceProvider;

/**
 *
 * @author harald
 */
@Stateless()
@ServiceMode(value = javax.xml.ws.Service.Mode.PAYLOAD)
@WebServiceProvider(serviceName = "CurrencyConvertor", portName = "CurrencyConvertorSoap", targetNamespace = "http://www.webserviceX.NET/", wsdlLocation = "WEB-INF/wsdl/www.webservicex.net/currencyconvertor.asmx.wsdl")
public class CurrencyConvertor implements javax.xml.ws.Provider<javax.xml.transform.Source> {
    
    @Override
    public javax.xml.transform.Source invoke(javax.xml.transform.Source source) {
        //TODO implement this method
        throw new UnsupportedOperationException("Not implemented yet.");
    }
    
}
